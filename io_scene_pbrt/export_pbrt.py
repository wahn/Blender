"""
This script is exports PBRT's scene description files.
"""

import os
import math
# Blender
import bpy

class PbrtExporter:
    def __init__(self):
        # filename related
        self.mainDir = None
        self.mainName = None
        # scene related
        self.scene = None
        self.scale_length = None
        # camera related
        self.camObj = None
        self.camData = None
        # motion blur related
        self.mBlur = []

    def createNewMaterial(self, obj):
        # create new material
        mat = bpy.data.materials.new(obj.name)
        print("WARNING: create new material %s" % (obj.name))
        # add the material to the object (not the data)
        bpy.data.meshes[obj.name].materials.append(mat)
        mat.specular_intensity = 0
        return mat

    def export(self, inFilename):
        head, tail = os.path.split(inFilename)
        self.mainDir = head
        root, ext = os.path.splitext(tail)
        self.mainName = root
        # scene
        self.scene = bpy.context.scene
        self.scene.world = None
        self.scale_length = self.scene.unit_settings.scale_length
        percent = self.scene.render.resolution_percentage / 100.0
        xresolution = int(self.scene.render.resolution_x * percent)
        yresolution = int(self.scene.render.resolution_y * percent)
        # motion blur
        use_motion_blur = self.scene.render.use_motion_blur
        animatedObjects = []
        animatedObjectNames = []
        motionMatrices = {}
        if use_motion_blur:
            shutter_start = 0.0
            shutter_end = self.scene.render.motion_blur_shutter
            motion_blur_samples = 32 # TODO: This setting is gone in Blender 2.80
            # loop through all objects
            for obj in self.scene.objects:
                if self.isVisible(obj) and self.isAnimated(obj):
                    animatedObjects.append(obj)
                    animatedObjectNames.append(obj.name)
        else:
            shutter_start = 0.0
            shutter_end = 0.0
            motion_blur_samples = 0
        self.mBlur = [use_motion_blur,
                      shutter_start,
                      shutter_end,
                      motion_blur_samples]
        # render camera
        self.camObj = self.scene.camera
        if self.camObj == None:
            print("WARNING: no camera, let me create one for you ;)")
            bpy.ops.object.camera_add()
            self.scene.camera = bpy.data.objects[bpy.data.cameras[0].name]
            bpy.ops.object.select_all(action='SELECT')
            bpy.ops.view3d.camera_to_view_selected()
        # motion blur
        if use_motion_blur:
            # collect matrices for all animated objects
            frame_current = self.scene.frame_current
            frame_step = 1.0 / float(motion_blur_samples - 1)
            for i in range(motion_blur_samples):
                subframe = shutter_end * frame_step * i
                self.scene.frame_set(frame_current, subframe=subframe)
                for objIdx in range(len(animatedObjects)):
                    obj = animatedObjects[objIdx]
                    # collect matrices (per object)
                    if i == 0:
                        motionMatrices[obj.name] = []
                    m = obj.matrix_world.copy()
                    inv = obj.matrix_world.copy()
                    inv.invert()
                    motionMatrices[obj.name].append([m, inv])
            # reset frame
            self.scene.frame_set(frame_current, subframe=0.0)
        # add animatedObjects and motionMatrices to self.mBlur for
        # usage below (in prepareExport(...))
        self.mBlur.append(animatedObjectNames)
        self.mBlur.append(motionMatrices)
        # lights
        if self.getNumlights() == 0:
            print("WARNING: no light emitters, let me create a point light for you ;)")
            bpy.ops.object.light_add(type='POINT', location=self.scene.camera.location)
            bpy.ops.object.select_by_type(type = 'LIGHT')
            if len(bpy.context.selected_objects):
                objName = bpy.context.selected_objects[0].name
                print(objName)
                objData = bpy.data.objects[objName].data
                objData.distance = 1
        # open
        outFilename = os.path.join(self.mainDir, self.mainName + ".pbrt")
        print('INFO: %s' % outFilename)
        self.pbrtFile = open(outFilename, "w")
        # header
        appString = "Blender %s.%s.%s" % bpy.app.version
        self.pbrtFile.write("# %s\n" % (self.mainName + ".pbrt"))
        self.pbrtFile.write("# exported from %s by Jan Walter's PBRT exporter\n\n" % appString)
        # coordinate system
        self.pbrtFile.write("Scale -1 1 1 # swap x-axis direction\n")
        # export camera(s)
        if self.camObj != None:
            self.camData = self.camObj.data
            if self.camData.type == "PERSP" or self.camData.type == "ORTHO":
                for obj in self.scene.objects:
                    data = obj.data
                    if obj.type == 'CAMERA':
                            transform = obj.matrix_world.copy()
                            self.writeCamera(obj,
                                             data.angle_x,
                                             data.angle_y,
                                             data.dof_object,
                                             data.dof_distance,
                                             data.cycles.aperture_size,
                                             [xresolution, yresolution],
                                             transform,
                                             obj == self.scene.camera)
        # motion blur related
        self.use_motion_blur = self.mBlur[0]
        self.shutter_start = self.mBlur[1]
        self.shutter_end = self.mBlur[2]
        self.motion_blur_samples = self.mBlur[3]
        self.animatedObjects = self.mBlur[4]
        self.motionMatrices = self.mBlur[5]
        if self.use_motion_blur:
            self.pbrtFile.write("TransformTimes %s %s\n" % (0, 1))
        # WorldBegin
        self.pbrtFile.write("WorldBegin\n")
        # export all materials
        self.writeMaterials()
        # export lights
        for obj in self.scene.objects:
            if (self.isVisible(obj)):
                data = obj.data
                if obj.type == 'LIGHT':
                    info = self.getInfoLight(obj)
                    color = info[0]
                    energy = info[1]
                    falloff = info[2]
                    use = info[3]
                    if data.type == 'AREA':
                        print("TODO: %s(%s)" % (obj.type, data.type))
                    elif data.type == 'HEMI':
                        print("TODO: %s(%s)" % (obj.type, data.type))
                    elif data.type == 'POINT':
                        transform = obj.matrix_world.copy()
                        self.writePointLight(obj.name, transform,
                                             info)
                    elif data.type == 'SPOT':
                        print("TODO: %s(%s)" % (obj.type, data.type))
                    elif data.type == 'SUN':
                        transform = obj.matrix_world.copy()
                        self.writeSunLight(obj.name, transform, info)
                    else:
                        print("WARNING: unknown LIGHT type %s(%s)",
                              (obj.type, data.type))
        # apply all modifiers (only for meshes)
        # bpy.ops.object.select_by_type(type = 'MESH')
        # if len(bpy.context.selected_objects):
        #     objName = bpy.context.selected_objects[0].name
        #     bpy.context.scene.objects.active = bpy.data.objects[objName]
        #     bpy.ops.object.convert()
        # export geometry
        numObjs = len(self.scene.objects)
        bpy.context.window_manager.progress_begin(0, numObjs)
        bpy.context.window_manager.progress_update(0)
        for index in range(numObjs):
            obj = self.scene.objects[index]
            self.exportObject(obj)
            bpy.context.window_manager.progress_update(index+1)
        bpy.context.window_manager.progress_end()
        # WorldEnd
        self.pbrtFile.write("WorldEnd\n")
        # close
        self.pbrtFile.close()

    def exportObject(self, obj):
        if self.isVisible(obj):
            data = obj.data
            if obj.type == 'CAMERA':
                pass
            elif obj.type == 'EMPTY':
                ##print("TODO: %s(%s)" % (obj.type, data))
                pass
            elif obj.type == 'LIGHT':
                pass
            elif obj.type == 'MESH':
                transform = obj.matrix_world.copy()
                # assume a material is in the first slot
                if len(obj.material_slots) >= 1:
                    mats = obj.material_slots.items()
                    matName = mats[0][0]
                    if matName != "":
                        mat = bpy.data.materials[matName]
                    else:
                        mat = self.createNewMaterial(obj)
                else:
                    mat = self.createNewMaterial(obj)
                info = self.getInfoMesh(obj)
                self.writeMesh(obj, transform.copy(), info)
            elif obj.type == 'SURFACE':
                transform = obj.matrix_world.copy()
                isPrimitive, primType = self.isPrimitive(obj)
                if isPrimitive:
                    print("WORK: '%s' is primitive: %s, %s" %
                          (obj.name, isPrimitive, primType))
                    # assume a material is in the first slot
                    if len(obj.material_slots) >= 1:
                        mats = obj.material_slots.items()
                        matName = mats[0][0]
                        if matName != "":
                            mat = bpy.data.materials[matName]
                        else:
                            mat = self.createNewMaterial(obj)
                    else:
                        mat = self.createNewMaterial(obj)
                    info = self.getInfoSurface(obj)
                    if primType == "cylinder":
                        self.writeCylinder(obj.name, transform.copy(),
                                       info, mat)
                    elif primType == "disk":
                        self.writeDisk(obj.name, transform.copy(),
                                       info, mat)
                    elif primType == "sphere":
                        self.writeSphere(obj.name, transform.copy(),
                                       info, mat)
                else:
                    # assume a material is in the first slot
                    if len(obj.material_slots) >= 1:
                        mats = obj.material_slots.items()
                        matName = mats[0][0]
                        if matName != "":
                            mat = bpy.data.materials[matName]
                        else:
                            mat = self.createNewMaterial(obj)
                    else:
                        mat = self.createNewMaterial(obj)
                    info = self.getInfoSurface(obj)
                    self.writeSurface(obj, transform.copy(), info)
            else:
                print("TODO: %s" % obj.type)

    def getInfoLight(self, obj):
        info = []
        data = obj.data
        # color
        if data.use_nodes:
            # assume there is an emission node with a color value
            if data.node_tree.nodes['Emission'].inputs['Color'].is_linked:
                print("WARNING: nodes['Emission'].inputs['Color'].is_linked")
                info.append([data.color[0], data.color[1], data.color[2]])
            else:
                # not linked to another node
                color = data.node_tree.nodes['Emission'].inputs['Color']
                r = color.default_value[0]
                g = color.default_value[1]
                b = color.default_value[2]
                a = color.default_value[3]
                info.append([r, g, b])
        else:
            info.append([data.color[0], data.color[1], data.color[2]])
        # energy
        if data.use_nodes:
            # assume there is an emission node with a strength value
            if data.node_tree.nodes['Emission'].inputs['Strength'].is_linked:
                print("WARNING: nodes['Emission'].inputs['Strength'].is_linked")
                info.append(data.energy)
            else:
                # not linked to another node
                strength = data.node_tree.nodes['Emission'].inputs['Strength']
                info.append(strength.default_value)
        else:
            info.append(data.energy)
        # falloff
        falloff = []
        try:
            falloff.append(data.falloff_type)
        except AttributeError:
            pass
        else:
            falloff.append(data.distance)
            if (data.falloff_type == 'CONSTANT' or
                data.falloff_type == 'INVERSE_LINEAR' or
                data.falloff_type == 'INVERSE_SQUARE'):
                # no additional info
                pass
            elif data.falloff_type == 'CUSTOM_CURVE':
                print("WARNING: falloff_type '%s' is not supported" %
                      data.falloff_type)
            elif data.falloff_type == 'LINEAR_QUADRATIC_WEIGHTED':
                falloff.append(data.linear_attenuation)
                falloff.append(data.quadratic_attenuation)
        info.append(falloff)
        # use flags
        use = []
        # # use_diffuse
        # use.append(data.use_diffuse)
        # # use_specular
        # use.append(data.use_specular)
        # use flags
        info.append(use)
        if data.type == 'SPOT':
            # spot parameters
            spot = []
            # spot_size
            spot_size = data.spot_size
            spot.append(spot_size)
            # spot_blend
            spot_blend = data.spot_blend
            spot.append(spot)
            # spot parameters
            info.append(spot)
        return info

    def getInfoMesh(self, obj):
        info = []
        # gather info here
        materials = []
        vertices = []
        vertexNormals = []
        polygons = []
        polygonNormals = []
        smoothPolygons = []
        polygonMaterialIndices = []
        uvs = []
        # convert to mesh and apply modifiers
        mesh = obj.to_mesh(bpy.context.depsgraph, apply_modifiers=True)
        mesh.calc_loop_triangles()
        # materials
        for i1 in range(len(mesh.materials)):
            material = mesh.materials[i1]
            if material != None:
                materials.append(material.name)
        if materials == []:
            for i1 in range(len(obj.material_slots)):
                material = obj.material_slots[i1]
                if material != None:
                    materials.append(material.name)
        info.append(materials)
        # vertices
        for i1 in range(len(mesh.vertices)):
            vertex = mesh.vertices[i1]
            vertices.append([vertex.co[0], vertex.co[1], vertex.co[2]])
            vertexNormals.append([vertex.normal[0],
                                  vertex.normal[1],
                                  vertex.normal[2]])
        info.append(vertices)
        info.append(vertexNormals)
        # triangles
        obj.update_from_editmode()
        for tri in mesh.loop_triangles:
            vertexIndices = []
            for i in range(3):
                vertexIndex = tri.vertices[i]
                vertexIndices.append(vertexIndex)
            polygons.append(vertexIndices)
            polygonNormals.append(tri.normal)
            smoothPolygons.append(tri.use_smooth)
            polygonMaterialIndices.append(tri.material_index)
        info.append(polygons)
        info.append(polygonNormals)
        info.append(smoothPolygons)
        info.append(polygonMaterialIndices)
        # remove intermediate mesh
        bpy.data.meshes.remove(mesh)
        return info

    def getNumlights(self):
        numLights = 0
        # lights
        for obj in self.scene.objects:
            if obj.type == 'LIGHT':
                if self.isVisible(obj):
                    light = obj.data
                    if light.users > 0:
                        numLights += 1
        # light emitters
        emitters = []
        for mat in bpy.data.materials:
            if mat.use_nodes:
                output = mat.node_tree.nodes['Material Output']
                surface = output.inputs['Surface']
                if self.nameStartsWith(surface.links[0].from_node.name, 'Emission'):
                    emission = mat.node_tree.nodes[surface.links[0].from_node.name]
                    power = emission.inputs['Strength'].default_value
                    if power > 0.0:
                        emitters.append(mat.name)
        for obj in self.scene.objects:
            if obj.type == 'MESH':
                info = self.getInfoMesh(obj)
                materials = info[0]
                for emitter in emitters:
                    if emitter in materials:
                        numLights += 1
        return numLights
        
    def getInfoSurface(self, obj):
        info = []
        data = obj.data
        # gather info here
        materials = []
        splines = []
        # materials
        for i1 in range(len(data.materials)): # data
            material = data.materials[i1]
            if material != None:
                materials.append(material.name)
        if materials == []:
            for i1 in range(len(obj.material_slots)):
                material = obj.material_slots[i1]
                if material != None:
                    materials.append(material.name)
        info.append(materials)
        # splines
        for i1 in range(len(data.splines)):
            spline = data.splines[i1]
            spline_info = []
            spline_info.append(spline.point_count_u)
            spline_info.append(spline.point_count_v)
            spline_info.append(spline.order_u)
            spline_info.append(spline.order_v)
            if not (spline.use_endpoint_u and spline.use_endpoint_v):
                print("WARNING: assuming spline uses endpoints");
            points = []
            for i2 in range(len(spline.points)):
                vertex = spline.points[i2]
                # weighted coordinates (4D)
                points.append([vertex.co[0], vertex.co[1], vertex.co[2], vertex.co[3]])
            spline_info.append(points)
            splines.append(spline_info)
        info.append(splines)
        # WORK
        return info

    def isAnimated(self, obj):
        if obj.animation_data != None:
            return True
        elif obj.parent != None:
            return self.isAnimated(obj.parent)
        return False

    def isPrimitive(self, obj):
        isPrimitive = False
        primType = None
        if (self.nameStartsWith(obj.data.name, "disk")):
            isPrimitive = True
            primType = "disk"
        elif (self.nameStartsWith(obj.data.name, "cylinder")):
            isPrimitive = True
            primType = "cylinder"
        elif (self.nameStartsWith(obj.data.name, "sphere")):
            isPrimitive = True
            primType = "sphere"
        return isPrimitive, primType

    def isVisible(self, obj):
        visible = obj.visible_get()
        return visible

    def nameStartsWith(self, name, pattern):
        retBool = False
        if len(name) >= len(pattern):
            words = name.split(pattern)
            if words[0] == "":
                retBool = True
        return retBool

    def writeArnoldSunSky(self, transform):
        xres = 4096
        yres = 2048
        # open .ass file
        outFilename = os.path.join(self.mainDir, "sun_sky.ass")
        assFile = open(outFilename, "w")
        # calculate elevation and azimuth
        m = transform
        pos, rot, scale = m.decompose()
        euler = rot.to_euler()
        elevation = 90.0 - math.degrees(euler.x)
        azimuth = 270.0 + math.degrees(euler.z) - 90.0
        # options
        assFile.write('options\n')
        assFile.write('{\n')
        assFile.write(' AA_samples 2\n')
        assFile.write(' outputs 1 1 STRING\n')
        assFile.write('  "RGBA RGBA filter driver"\n')
        assFile.write(' xres %s\n' % xres)
        assFile.write(' yres %s\n' % yres)
        assFile.write(' camera "%s"\n' % "CAbake")
        assFile.write(' background "%s"\n' % "sun_sky")
        assFile.write('}\n')
        # cyl_camera
        assFile.write('spherical_camera\n')
        assFile.write('{\n')
        assFile.write(' name %s\n' % "CAbake")
        assFile.write(' position %s %s %s\n' % (0, 0, 0))
        assFile.write(' look_at %s %s %s\n' % (0, -1, 0))
        assFile.write(' up %s %s %s\n' % (0, 0, 1))
        assFile.write('}\n')
        # filter
        assFile.write('gaussian_filter\n')
        assFile.write('{\n')
        assFile.write(' name filter\n')
        assFile.write('}\n')
        # driver
        assFile.write('driver_exr\n')
        assFile.write('{\n')
        assFile.write(' name driver\n')
        assFile.write(' filename "sun_sky.exr"\n')
        assFile.write('}\n')
        # physical_sky
        assFile.write('physical_sky\n')
        assFile.write('{\n')
        assFile.write(' name sun_sky\n')
        assFile.write(' turbidity 2\n')
        assFile.write(' elevation  %s\n' % elevation)
        assFile.write(' azimuth %s\n' % azimuth)
        assFile.write(' sun_direction 0 1 0\n')
        assFile.write(' intensity 1\n')
        assFile.write(' X 1 0 0\n')
        assFile.write(' Y 0 0 1\n')
        assFile.write(' Z 0 1 0\n')
        assFile.write('}\n')
        # skydome_light
        assFile.write('skydome_light\n')
        assFile.write('{\n')
        assFile.write(' name sun_skydome\n')
        assFile.write(' color sun_sky\n')
        assFile.write(' intensity 1\n')
        assFile.write(' samples 6\n')
        assFile.write(' resolution %s\n' % xres)
        assFile.write('}\n')
        # close .ass file
        assFile.close()
        
    def writeBlenderMaterial(self, mat):
        if mat.use_nodes:
            self.pbrtFile.write('  # TODO: material "%s" uses nodes\n' % mat.name)
        else:
            self.pbrtFile.write('  MakeNamedMaterial "%s"\n' % mat.name)
            # matte
            self.pbrtFile.write('    "string type" [ "%s" ]\n' % "matte")
            Kd = mat.diffuse_color
            self.pbrtFile.write('    "color Kd" [ %s %s %s ]\n' % (Kd[0], Kd[1], Kd[2]))
        
    def writeCamera(self, obj, angle_x, angle_y, dof_object, dof_distance,
                    aperture_size, resolution, transform,
                    isRenderCamera):
        if isRenderCamera:
            self.pbrtFile.write("# %s\n" % obj.name)
            m = transform
            matrix = m.transposed()
            pos = matrix[3]
            forwards = -matrix[2]
            target = (pos + forwards)
            up = matrix[1]
            self.pbrtFile.write("LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.pbrtFile.write("       %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.pbrtFile.write("       %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            aspect = resolution[0] / float(resolution[1])
            if obj.data.type == "ORTHO":
                self.pbrtFile.write('Camera "orthographic"\n')
                swy = obj.data.ortho_scale / (2.0 * aspect)
                swx = aspect * swy
                self.pbrtFile.write('  "float screenwindow" [ -%s %s -%s %s ]\n' %
                                    (swx, swx, swy, swy))
            if obj.data.type == "PERSP":
                self.pbrtFile.write('Camera "perspective"\n')
                if aspect > 1.0:
                    fov = math.degrees(angle_y)
                else:
                    fov = math.degrees(angle_x)
                self.pbrtFile.write('  "float fov" [ %s ]\n' % fov)
            if aperture_size > 0.0 and (dof_distance > 0.0 or dof_object != None):
                focaldistance = dof_distance
                if dof_object != None:
                    # measure distance between DOF object and camera
                    v = dof_object.location - obj.location
                    focaldistance = v.length
                self.pbrtFile.write('  "float lensradius" [ %s ]\n' % aperture_size)
                self.pbrtFile.write('  "float focaldistance" [ %s ]\n' % focaldistance)
            obj = bpy.data.objects[obj.name]
            data = obj.data
            self.pbrtFile.write('Film "image"\n')
            self.pbrtFile.write('  "integer xresolution" [ %s ]\n' %
                               resolution[0])
            self.pbrtFile.write('  "integer yresolution" [ %s ]\n' %
                               resolution[1])
            pixelsamples = 1
            if self.scene.render.use_antialiasing:
                pixelsamples = int(self.scene.render.antialiasing_samples)
            self.pbrtFile.write('Sampler "sobol" "integer pixelsamples" [%s]\n' % pixelsamples)
            if self.scene.render.engine == "BLENDER_EEVEE":
                self.pbrtFile.write('Integrator "directlighting"\n')
            else:
                self.pbrtFile.write('Integrator "bdpt"\n')
        else:
            print("TODO: export camera %s with comments" % obj.name)
        
    def writeCylinder(self, name, transform, info, mat):
        # info
        materials = info[0]
        splines = info[1]
        # ignore
        point_count_u = splines[0][0]
        point_count_v = splines[0][1]
        order_u = splines[0][2]
        order_v = splines[0][3]
        # points (CVs)
        points = splines[0][4]
        # for now just assume last point's x-coordinate
        radius = points[-1][0] 
        # name
        self.pbrtFile.write("  # %s\n" % name)
        # AttributeBegin
        self.pbrtFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        self.pbrtFile.write('    Transform [\n')
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.pbrtFile.write('    ]\n')
        matIndex = 0 # TODO: Can there be more than one material?
        matName = materials[matIndex]
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        # Shape
        self.pbrtFile.write('    Shape "cylinder" "float radius" [ %s ]\n' % radius)
        # AttributeEnd
        self.pbrtFile.write("  AttributeEnd\n")
       
    def writeDisk(self, name, transform, info, mat):
        # info
        materials = info[0]
        splines = info[1]
        # ignore
        point_count_u = splines[0][0]
        point_count_v = splines[0][1]
        order_u = splines[0][2]
        order_v = splines[0][3]
        # points (CVs)
        points = splines[0][4]
        # for now just assume last point's x-coordinate
        radius = points[-1][0] 
        # name
        self.pbrtFile.write("  # %s\n" % name)
        # AttributeBegin
        self.pbrtFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        self.pbrtFile.write('    Transform [\n')
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.pbrtFile.write('    ]\n')
        matIndex = 0 # TODO: Can there be more than one material?
        matName = materials[matIndex]
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        # Shape
        self.pbrtFile.write('    Shape "disk" "float radius" [ %s ]\n' % radius)
        # AttributeEnd
        self.pbrtFile.write("  AttributeEnd\n")
       
    def writeSphere(self, name, transform, info, mat):
        # info
        materials = info[0]
        splines = info[1]
        # ignore
        point_count_u = splines[0][0]
        point_count_v = splines[0][1]
        order_u = splines[0][2]
        order_v = splines[0][3]
        # points (CVs)
        points = splines[0][4]
        # for now just assume 17th point's x-coordinate
        radius = points[16][0] 
        # name
        self.pbrtFile.write("  # %s\n" % name)
        # AttributeBegin
        self.pbrtFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        self.pbrtFile.write('    Transform [\n')
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.pbrtFile.write('    ]\n')
        matIndex = 0 # TODO: Can there be more than one material?
        matName = materials[matIndex]
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        # Shape
        self.pbrtFile.write('    Shape "sphere" "float radius" [ %s ]\n' % radius)
        # AttributeEnd
        self.pbrtFile.write("  AttributeEnd\n")
       
    def writeMaterials(self):
        for mat in bpy.data.materials:
            self.pbrtFile.write("  # %s\n" % mat.name)
            self.writeBlenderMaterial(mat)

    def writeMesh(self, obj, transform, info):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        # hasUVs, uvs = info[7]
        # name
        self.pbrtFile.write("  # %s\n" % obj.name)
        # check dupli_type
        useInstance = False
        if obj.parent != None:
            if obj.parent.instance_type == 'VERTS':
                useInstance = True
        if useInstance:
            # ObjectBegin
            self.pbrtFile.write("  ObjectBegin \"%s\"\n" % obj.name)
        else:
            # AttributeBegin
            self.pbrtFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        self.pbrtFile.write('    Transform [\n')
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.pbrtFile.write('    ]\n')
        # for each material ...
        for matIndex in range(len(materials)):
            if matIndex in polygonMaterialIndices:
                matName = materials[matIndex]
                # NamedMaterial
                mat = bpy.data.materials[matName]
                if mat.use_nodes:
                    output = mat.node_tree.nodes['Material Output']
                    surface = output.inputs['Surface']
                    if self.nameStartsWith(surface.links[0].from_node.name, 'Emission'):
                        emission = mat.node_tree.nodes[surface.links[0].from_node.name]
                        color = emission.inputs['Color']
                        # copy color from node to base color
                        mat.diffuse_color[0] = color.default_value[0]
                        mat.diffuse_color[1] = color.default_value[1]
                        mat.diffuse_color[2] = color.default_value[2]
                        # AreaLightSource
                        self.pbrtFile.write('    AreaLightSource "%s"\n' % "area")
                        power = emission.inputs['Strength'].default_value
                        L = mat.diffuse_color
                        self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                                            (L[0] * power, L[1] * power, L[2] * power))
                else:
                    self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
                # if there's a single smooth polygon, we export normals
                useVertexNormals = False
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        smoothPolygon = smoothPolygons[pi]
                        if smoothPolygon:
                            useVertexNormals = True
                            break
                # mesh
                self.pbrtFile.write('    Shape "trianglemesh"\n')
                self.pbrtFile.write('      "point P" [\n')
                for v in vertices:
                    self.pbrtFile.write('        %s %s %s\n' %
                                        (self.scale_length * v[0],
                                         self.scale_length * v[1],
                                         self.scale_length * v[2]))
                self.pbrtFile.write('      ]\n')
                self.pbrtFile.write('      "integer indices" [\n')
                for pi in range(len(polygons)):
                    polygon = polygons[pi]
                    mi = polygonMaterialIndices[pi]
                    if matIndex == mi:
                        if len(polygon) == 3:
                            self.pbrtFile.write('        %s %s %s\n' % (polygon[0], polygon[1], polygon[2]))
                        elif len(polygon) == 4:
                            self.pbrtFile.write('        %s %s %s\n' % (polygon[0], polygon[1], polygon[2]))
                            self.pbrtFile.write('        %s %s %s\n' % (polygon[0], polygon[2], polygon[3]))
                        else:
                            print("ERROR: writeMesh(\"%s\"), quads or triangles expected" % obj.name)
                self.pbrtFile.write('      ]\n')
                if useVertexNormals:
                    self.pbrtFile.write('      "normal N" [\n')
                    for n in vertexNormals:
                        self.pbrtFile.write('        %s %s %s\n' % (n[0], n[1], n[2]))
                    self.pbrtFile.write('      ]\n')
        if useInstance:
            # ObjectEnd
            self.pbrtFile.write("  ObjectEnd\n")
        else:
            # AttributeEnd
            self.pbrtFile.write("  AttributeEnd\n")
        if useInstance:
            info = self.getInfoMesh(obj.parent)
            vertices = info[1]
            # name
            self.pbrtFile.write("  # %s\n" % obj.parent.name)
            for v in vertices:
                # AttributeBegin
                self.pbrtFile.write("  AttributeBegin\n")
                # Translate
                self.pbrtFile.write("    Translate %s %s %s\n" % (v[0], v[1], v[2]))
                # ObjectInstance
                self.pbrtFile.write("    ObjectInstance \"%s\"\n" % obj.name)
                # AttributeEnd
                self.pbrtFile.write("  AttributeEnd\n")

    def writePointLight(self, name, transform, info):
        self.pbrtFile.write('  # %s\n' % name)
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        # use_diffuse = info[3][0]
        # use_specular = info[3][1]
        # AttributeBegin
        self.pbrtFile.write("  AttributeBegin\n")
        # LightSource
        loc, rot, scale = transform.decompose()
        self.pbrtFile.write('    LightSource "point"\n')
        self.pbrtFile.write('      "color scale" [ %s %s %s ]\n' %
                            (energy, energy, energy))
        self.pbrtFile.write('      "color I" [ %s %s %s ]\n' %
                            (color[0], color[1], color[2]))
        self.pbrtFile.write('      "point from" [ %s %s %s ]\n' %
                            (loc[0], loc[1], loc[2]))
        # AttributeEnd
        self.pbrtFile.write("  AttributeEnd\n")

    def writeSunLight(self, name, transform, info):
        if name == 'sun' or name == 'Sun':
            self.writeArnoldSunSky(transform)
        self.pbrtFile.write('  # %s\n' % name)
        # info
        color = info[0]
        # energy = info[1]
        # falloff = info[2]
        # use_diffuse = info[3][0]
        # use_specular = info[3][1]
        # AttributeBegin
        self.pbrtFile.write("  AttributeBegin\n")
        # LightSource
        loc, rot, scale = transform.decompose()
        self.pbrtFile.write('    LightSource "distant"\n')
        self.pbrtFile.write('      "color L" [ %s %s %s ]\n' %
                            (color[0], color[1], color[2]))
        self.pbrtFile.write('      "point from" [ %s %s %s ]\n' %
                            (loc[0], loc[1], loc[2]))
        self.pbrtFile.write('      "point to" [ %s %s %s ]\n' %
                            (0.0, 0.0, 0.0))
        # AttributeEnd
        self.pbrtFile.write("  AttributeEnd\n")

    def writeSurface(self, obj, transform, info):
        # info
        materials = info[0]
        splines = info[1]
        # name
        self.pbrtFile.write("  # %s\n" % obj.name)
        # AttributeBegin
        self.pbrtFile.write("  AttributeBegin\n")
        # motion blur
        if self.use_motion_blur and (obj.name in self.animatedObjects):
            self.pbrtFile.write("    ActiveTransform StartTime\n")
        # Transform
        m = transform
        self.pbrtFile.write('    Transform [\n')
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.pbrtFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.pbrtFile.write('    ]\n')
        # motion blur
        if self.use_motion_blur and (obj.name in self.animatedObjects):
            # second Transform (use the last one)
            matrices = self.motionMatrices[obj.name]
            m = matrices[-1][0]
            self.pbrtFile.write("    ActiveTransform EndTime\n")
            self.pbrtFile.write('    Transform [\n')
            self.pbrtFile.write('      %s %s %s %s\n' %
                               (m[0][0],
                                m[1][0],
                                m[2][0],
                                m[3][0]))
            self.pbrtFile.write('      %s %s %s %s\n' %
                               (m[0][1],
                                m[1][1],
                                m[2][1],
                                m[3][1]))
            self.pbrtFile.write('      %s %s %s %s\n' %
                               (m[0][2],
                                m[1][2],
                                m[2][2],
                                m[3][2]))
            self.pbrtFile.write('      %s %s %s %s\n' %
                               (self.scale_length * m[0][3],
                                self.scale_length * m[1][3],
                                self.scale_length * m[2][3],
                                m[3][3]))
            self.pbrtFile.write('    ]\n')
            self.pbrtFile.write("    ActiveTransform All\n")
        # use Blender materials (old raytracer, not Cycles)
        matIndex = 0 # TODO: Can there be more than one material?
        matName = materials[matIndex]
        self.pbrtFile.write('    NamedMaterial "%s"\n' % matName)
        # mesh
        splineIndex = 0 # TODO: Can there be more than one spline?
        spline_info = splines[splineIndex]
        # unpack spline info
        nu = spline_info[0]
        nv = spline_info[1]
        uorder = spline_info[2]
        vorder = spline_info[3]
        cvs = spline_info[4]
        self.pbrtFile.write('    Shape "nurbs"\n')
        self.pbrtFile.write('      "integer nu" [ %s ]\n' % nu)
        self.pbrtFile.write('      "integer nv" [ %s ]\n' % nv)
        self.pbrtFile.write('      "integer uorder" [ %s ]\n' % uorder)
        self.pbrtFile.write('      "integer vorder" [ %s ]\n' % vorder)
        # TODO: calculate knots (we assume nu == nv == 4)
        if uorder == 3:
            self.pbrtFile.write('      "float uknots" [ 0.0 0.0 0.0 0.5 1.0 1.0 1.0 ]\n')
        elif uorder == 4:
            self.pbrtFile.write('      "float uknots" [ 0.0 0.0 0.0 0.0 1.0 1.0 1.0 1.0 ]\n')
        else:
            print("WARNING: TODO: calculate knots for uorder == %s" % uorder)
        if vorder == 3:
            self.pbrtFile.write('      "float vknots" [ 0.0 0.0 0.0 0.5 1.0 1.0 1.0 ]\n')
        elif vorder == 4:
            self.pbrtFile.write('      "float vknots" [ 0.0 0.0 0.0 0.0 1.0 1.0 1.0 1.0 ]\n')
        else:
            print("WARNING: TODO: calculate knots for vorder == %s" % vorder)
        self.pbrtFile.write('      "float u0" [ 0.0 ]\n')
        self.pbrtFile.write('      "float v0" [ 0.0 ]\n')
        self.pbrtFile.write('      "float u1" [ 1.0 ]\n')
        self.pbrtFile.write('      "float v1" [ 1.0 ]\n')
        self.pbrtFile.write('      "point P" [\n')
        for v in cvs:
            self.pbrtFile.write('        %s %s %s\n' %
                                (self.scale_length * v[0],
                                 self.scale_length * v[1],
                                 self.scale_length * v[2]))
        self.pbrtFile.write('      ]\n')
        # AttributeEnd
        self.pbrtFile.write("  AttributeEnd\n")

def save(context, filepath = ""):
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode = 'OBJECT')
    # print a usage message
    print("=" * 79)
    print("bpy.ops.export_scene.pbrt(filepath = \"%s\")" % filepath)
    # delegate work to exporter class
    exporter = PbrtExporter()
    exporter.export(filepath)
    return {'FINISHED'}
