# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>

bl_info = {
    "name": "Export to PBRT renderer",
    "author": "Jan Walter",
    "version": (0, 4, 5),
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "description": "Exports a scene in .pbrt file format",
    "warning": "",
    "wiki_url": "https://codeberg.org/wahn/Blender/wiki",
    "tracker_url": "",
    "category": "Import-Export"}

import bpy
from bpy.props import StringProperty
from bpy_extras.io_utils import ExportHelper

class ExportPbrt(bpy.types.Operator, ExportHelper):
    """Save a PBRT File"""

    bl_idname = "export_scene.pbrt"
    bl_label = "Export PBRT"
    bl_options = {'PRESET'}
    filename_ext = ".pbrt"
    filter_glob = StringProperty(default = "*.pbrt", options = {'HIDDEN'})
    
    def execute(self, context):
        keywords = self.as_keywords(ignore = (
                "check_existing",
                "filter_glob"))
        from . import export_pbrt
        return export_pbrt.save(context, **keywords)

def menu_func_export(self, context):
    self.layout.operator(ExportPbrt.bl_idname, 
                         text = "PBRT Renderer (.pbrt)")

classes = (
    ExportPbrt,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()
