import bpy

# which Blender version?
if bpy.app.version[0] == 2 and bpy.app.version[1] > 79:
    v2_80 = True
else:
    v2_80 = False
# delete scene
bpy.ops.object.select_all(action='SELECT')
if v2_80:
    bpy.ops.object.delete(use_global=True, confirm=True)
else:
    bpy.ops.object.delete(use_global=True)
# create new mesh
bpy.ops.object.add(type='MESH')
# name things
obj = bpy.data.objects[0]
obj.name = "Triangle"
obj.data.name = "Triangle"
# add vertices
obj.data.vertices.add(3)
obj.data.vertices[1].co[0] = 1
obj.data.vertices[2].co[1] = 1
# enter edit mode
bpy.ops.object.editmode_toggle()
# select all three vertices
bpy.ops.mesh.select_all(action='SELECT')
# add the triangle face
bpy.ops.mesh.edge_face_add()
# enter object mode
bpy.ops.object.editmode_toggle()
