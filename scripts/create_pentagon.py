import bpy
import math

# which Blender version?
if bpy.app.version[0] == 2 and bpy.app.version[1] > 79:
    v2_80 = True
else:
    v2_80 = False
# delete scene
bpy.ops.object.select_all(action='SELECT')
if v2_80:
    bpy.ops.object.delete(use_global=True, confirm=True)
else:
    bpy.ops.object.delete(use_global=True)
# create new mesh
bpy.ops.object.add(type='MESH')
# name things
obj = bpy.data.objects[0]
obj.name = "Pentagon"
obj.data.name = "Pentagon"
# add vertices
obj.data.vertices.add(1)
obj.data.vertices[0].co[0] = 1
# enter edit mode
bpy.ops.object.editmode_toggle()
# spin vertex
bpy.ops.mesh.spin(steps=5, angle=2*math.pi, axis=(0,0,1))
# select all five vertices
bpy.ops.mesh.select_all(action='SELECT')
# add single face
bpy.ops.mesh.edge_face_add()
# triangulate face
bpy.ops.mesh.quads_convert_to_tris(quad_method='BEAUTY', ngon_method='BEAUTY')
# enter object mode
bpy.ops.object.editmode_toggle()
# export twice to create camera and light
bpy.ops.export_scene.pbrt(filepath = "/home/jan/tmp/rs_pbrt/pentagon.pbrt")
bpy.ops.export_scene.pbrt(filepath = "/home/jan/tmp/rs_pbrt/pentagon.pbrt")
