Let's create some [Python][python] scripts for [Blender][blender] to
export to [rs_pbrt][rs_pbrt].

[python]: https://www.python.org
[blender]: https://www.blender.org
[rs_pbrt]: https://codeberg.org/wahn/rs_pbrt
